package org.levelup.stack;

public class Stack {
    private class Element {
        private String value;
        private Element next;
        Element(String value) {
            this.value = value;
        }
    }

    private Element head;

    public void push(String value) {
        Element el = new Element(value);
        if (head == null) {
            head = el;
        } else {
            el.next = head;
            head = el;
        }
    }

    public String pop() {
        if (head == null) {
            return null;
        } else {
            String value = head.value;
            head = head.next;
            return value;
        }
    }
}
