package org.levelup.stack;

import org.junit.jupiter.api.*;

public class ClassTest {

    @BeforeAll
    public static void once() {
        System.out.println("first");
    }

    @BeforeEach
    public void setup() {
        System.out.println("before each test");
    }

    @Test
    public void test(){
        System.out.println("test");
        assert true;
    }

    @AfterEach
    public void clear(){
        System.out.println("after each test");
    }

    @AfterAll
    public static void tearDown(){
        System.out.println("last");
    }
}
