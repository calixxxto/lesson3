package org.levelup.stack;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class BracersTest {

    private Bracers bracers;

    @BeforeEach
    public void setup(){
        this.bracers = new Bracers();
    }

    @Test
    @DisplayName("When value is null, then throw exception")
    public void testVerify_valueIsNull_throwException(){
        Assertions.assertThrows(IllegalArgumentException.class, () -> bracers.verify(null));
    }

    @Test
    public void testVerify_validString_returnTrue() {
        boolean result = bracers.verify("[]");
        Assertions.assertTrue(result);
    }

}
